// ES6
// // console.log(7**3)

// let from = "Ms thonie"
// console.log('Hi sirs and maam \n Hello sirs and maam');
// console.log(`Hello
// Hello`)

// console.log(`Hello Batch 145 love, ${from}`)

// bobo ng ZUITT


let grades = [89,87,78,96];

let fullName = ['Juan', 'Dela', 'Cruz'];

console.log(fullName[0])
console.log(fullName[1])

const [first,middle,last] = fullName
console.log(last)
console.log(`Hello ${first} ${middle} ${last} ! It's nice to meet you!`)

function addAndSubtract(a,b) {
	return [a+b, a-b]
}

const array = addAndSubtract(4,2)
console.log(array)
const [sum, diff] = addAndSubtract(4,2)

console.log(sum)
console.log(diff)

const letters = ['A', 'B', 'C', 'D', 'E'];
const [a, ...rest] = letters;

console.log(a)
console.log(rest)

const person = {
	givenName: 'jane',
	maidenName: 'Dela',
	familyName: 'Cruz',
};

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// object destructuring
const {givenName: name,maidenName,familyName} = person
console.log(`Hello my name is ${name} ${maidenName} ${familyName}`)

function printUser({givenName: nickname, familyName}) {
	console.log(`I am ${nickname} ${familyName}`)
}

printUser(person)

// const person2 = {
// 	"given Name": 'james',
// 	"last Name": 'bond',

// };

// console.log(person2['given Name'])


const hello = function() {
	console.log('Hello world')
}

// arrow function

const helloAgain = () => console.log('hello world');

const add = (x,y) => x + y
const total = add(1,2);
console.log(total)

const students = ['Joey', 'Jade', 'Judy']

// students.forEach(function(student) {
// 	console.log(`${student} is a student`)
// })

students.forEach(student => console.log(`${student} is a student`))

let numbers = [1,2,3,4,5];
let numberMap = numbers.map(num => {
	return num * num
})
let numberFilter = numbers.filter(num => {
	return num >2
})
console.log(numberFilter)

////////////////////////////////////////////////////

const greet = (name = 'bob') => {
	return `Good morning, ${name}`
}

console.log(greet('Anna'))
console.log(greet())

const person3 = {
	namE: 'Jill',
	age: 27,
	address: {
		city: 'Some city',
		street: 'Some street'
	}
}

const {namE, age} = person3
console.log(namE, age)

////////////////////////////////////////
// allows creation of objects using classes as blueprints

class Car {
	constructor(brand, name, year) {
		this.brand = brand
		this.name = name
		this.year = year
	}
}

const toyota = new Car('Toyota', 'inova', 2012)
console.log(toyota)